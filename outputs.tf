output "s3" {
  value       = aws_s3_bucket.s3
  description = "S3 Bucket"
}
