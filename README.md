# Bucket Module

This module is capable to generate an S3 blocked bucket.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - bucket name
- `cors_enabled` - enable/disable CORS policy (optional, default to false)

Usage
-----

```hcl
module "bucket" {
  source        = "git::https://gitlab.com/mesabg-tfmodules/bucket.git"

  environment   = "environment"

  name          = "bucket name"
  cors_enabled  = true
}
```

Outputs
=======

 - `s3` - Created S3 bucket


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
