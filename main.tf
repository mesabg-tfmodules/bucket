terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

resource "aws_s3_bucket" "s3" {
  bucket            = var.name

  tags = {
    Name            = var.name
    Environment     = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_s3_bucket_cors_configuration" "cors_configuration" {
  count   = var.cors_enabled ? 1 : 0
  bucket  = aws_s3_bucket.s3.bucket

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 300
  }
}

resource "aws_s3_bucket_public_access_block" "s3_bucket_public_access_block" {
  count                   = var.public_access ? 0 : 1
  bucket                  = aws_s3_bucket.s3.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
