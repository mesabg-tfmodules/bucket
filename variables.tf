variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Bucket name"
}

variable "cors_enabled" {
  type        = bool
  description = "Enable CORS policy for the bucket"
  default     = false 
}

variable "public_access" {
  type        = bool
  description = "Enable or Disable public access"
  default     = false
}
